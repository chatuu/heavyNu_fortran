  
 |-----------------------|
 | Printing Zeroth Norms |
 |-----------------------|
  
 
 
 
 0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0
 0o0o0o                             o0o0o0
 0o0o0o      Performing Analysis    o0o0o0
 0o0o0o    And Filling Histograms   o0o0o0
 0o0o0o                             o0o0o0
 0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0
 
 
 
 
|=============================|
|===   Working type  1:    ===|
|===        CCDIS          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/ccbabygen                                                                                  
 
 Working on file   1 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.001.h                                   Working on file   2 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.002.h                                   Working on file   3 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.003.h                                   Working on file   4 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.004.h                                   Working on file   5 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.005.h                                   Working on file   6 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.006.h                                   Working on file   7 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.007.h                                   Working on file   8 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.008.h                                   Working on file   9 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.009.h                                   Working on file  10 of  10: nts/babynt_4trk-2mu/genie_mc/ccdis/babynt_le4trk_2mu_ccgenie.010.h                                   
 
 
|=============================|
|===   Working type  2:    ===|
|===        NCDIS          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/ncbabygen                                                                                  
 
 Working on file   1 of   2: nts/babynt_4trk-2mu/genie_mc/ncdis/babynt_le4trk_2mu_ncgenie.001.h                                   Working on file   2 of   2: nts/babynt_4trk-2mu/genie_mc/ncdis/babynt_le4trk_2mu_ncgenie.002.h                                   
 
 
|=============================|
|===   Working type  3:    ===|
|===         JPsi          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/jpsi-mumu                                                                                  
 
 Working on file   1 of   4: nts/fullnt/jpsi_mumu/cohjpsi-mumu.001.1.h                                                            Working on file   2 of   4: nts/fullnt/jpsi_mumu/cohjpsi-mumu.001.2.h                                                            Working on file   3 of   4: nts/fullnt/jpsi_mumu/cohjpsi-mumu.001.3.h                                                            Working on file   4 of   4: nts/fullnt/jpsi_mumu/cohjpsi-mumu.001.4.h                                                            
 
 
|=============================|
|===   Working type  4:    ===|
|===        OBG            ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/obgbaby                                                                                    
 
 Working on file   1 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.001.h                                                             Working on file   2 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.002.h                                                             Working on file   3 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.003.h                                                             Working on file   4 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.004.h                                                             Working on file   5 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.005.h                                                             Working on file   6 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.006.h                                                             Working on file   7 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.007.h                                                             Working on file   8 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.008.h                                                             Working on file   9 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.009.h                                                             Working on file  10 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.010.h                                                             Working on file  11 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.011.h                                                             Working on file  12 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.012.h                                                             Working on file  13 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.013.h                                                             Working on file  14 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.014.h                                                             Working on file  15 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.015.h                                                             Working on file  16 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.016.h                                                             Working on file  17 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.017.h                                                             Working on file  18 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.018.h                                                             Working on file  19 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.019.h                                                             Working on file  20 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.020.h                                                             Working on file  21 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.021.h                                                             Working on file  22 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.022.h                                                             Working on file  23 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.023.h                                                             Working on file  24 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.024.h                                                             Working on file  25 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.025.h                                                             Working on file  26 of  26: nts/babynt_4trk-2mu/obg/babynt_obg.026.h                                                             
 
 
|=============================|
|===   Working type  5:    ===|
|===       CohPi+          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/cohpipnuabs                                                                                
 
 Working on file   1 of   3: nts/fullnt/nuage_mc/cohpip/bs/cohpip_bs_nuage_0_1.paw                                                Working on file   2 of   3: nts/fullnt/nuage_mc/cohpip/bs/cohpip_bs_nuage_0_2.paw                                                Working on file   3 of   3: nts/fullnt/nuage_mc/cohpip/bs/cohpip_bs_nuage_0_3.paw                                                
 
 
|=============================|
|===   Working type  6:    ===|
|===      CohRho+          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/cohrhop                                                                                    
 
 Working on file   1 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.01.h                                                            Working on file   2 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.02.h                                                            Working on file   3 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.03.h                                                            Working on file   4 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.04.h                                                            Working on file   5 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.05.h                                                            Working on file   6 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.06.h                                                            Working on file   7 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.07.h                                                            Working on file   8 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.08.h                                                            Working on file   9 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.09.h                                                            Working on file  10 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.10.h                                                            Working on file  11 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.11.h                                                            Working on file  12 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.12.h                                                            Working on file  13 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.13.h                                                            Working on file  14 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.14.h                                                            Working on file  15 of  15: nts/fullnt/cohrhop/cohrhop_r0.3_.001.15.h                                                            
 
 
|=============================|
|===   Working type  7:    ===|
|===      aNuMu CC         ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/amccbabygen                                                                                
 
 Working on file   1 of   1: nts/babynt_4trk-2mu/genie_mc/anumucc/babynt_le4trk_2mu_anmccgenie.001.h                              
 
 
|=============================|
|===   Working type  8:    ===|
|===         QE            ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/qeccbabygen                                                                                
 
 Working on file   1 of   1: nts/babynt_4trk-2mu/genie_mc/qecc/babynt_le4trk_2mu_qegenie.001.h                                    
 
 
|=============================|
|===   Working type  9:    ===|
|===       CohPi0          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/cohpi0nuabk                                                                                
 
 Working on file   1 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_01.paw                                           Working on file   2 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_02.paw                                           Working on file   3 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_03.paw                                           Working on file   4 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_04.paw                                           Working on file   5 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_05.paw                                           Working on file   6 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_06.paw                                           Working on file   7 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_07.paw                                           Working on file   8 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_09.paw                                           Working on file   9 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_10.paw                                           Working on file  10 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_11.paw                                           Working on file  11 of  11: nts/fullnt/nuage_mc/cohpi0/bk/cohpi0_bk_nuage_set35_12.paw                                           
 
 
|=============================|
|===   Working type 10:    ===|
|===       Nue CC          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/neccbabygen                                                                                
 
 Working on file   1 of   1: nts/babynt_4trk-2mu/genie_mc/nuecc/babynt_le4trk_2mu_nueccgenie.001.h                                
 
 
|=============================|
|===   Working type 11:    ===|
|===      aNue CC          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/aneccgen                                                                                   
 
 Working on file   1 of   5: nts/fullnt/genie_mc/anuecc/ccdis_genie_21214200_01.paw                                               Working on file   2 of   5: nts/fullnt/genie_mc/anuecc/ccdis_genie_21214200_02.paw                                               Working on file   3 of   5: nts/fullnt/genie_mc/anuecc/ccdis_genie_21214200_03.paw                                               Working on file   4 of   5: nts/fullnt/genie_mc/anuecc/ccdis_genie_21214200_04.paw                                               Working on file   5 of   5: nts/fullnt/genie_mc/anuecc/ccdis_genie_21214200_05.paw                                               
 
 
|=============================|
|===   Working type 12:    ===|
|===      aNuMu NC         ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/anmncgen                                                                                   
 
 Working on file   1 of   5: nts/fullnt/genie_mc/anumunc/ncdis_genie_21212100_01.paw                                              Working on file   2 of   5: nts/fullnt/genie_mc/anumunc/ncdis_genie_21212100_02.paw                                              Working on file   3 of   5: nts/fullnt/genie_mc/anumunc/ncdis_genie_21212100_03.paw                                              Working on file   4 of   5: nts/fullnt/genie_mc/anumunc/ncdis_genie_21212100_04.paw                                              Working on file   5 of   5: nts/fullnt/genie_mc/anumunc/ncdis_genie_21212100_05.paw                                              
 
 
|=============================|
|===   Working type 13:    ===|
|===      CohRho0          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/cohrho0                                                                                    
 
 Working on file   1 of   1: nts/fullnt/cohrho0/cohrho0_30000events.h                                                             
 
 
|=============================|
|===   Working type 14:    ===|
|===        Res            ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/resbabygen                                                                                 
 
 Working on file   1 of   1: nts/babynt_4trk-2mu/genie_mc/rescc/babynt_le4trk_2mu_resgenie.001.h                                  
 
 
|=============================|
|===   Working type 15:    ===|
|===      CohPhi0          ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/cohphi0                                                                                    
 
 Working on file   1 of   1: nts/fullnt/cohphi0/cohphi0.001.1.h                                                                   
 
 
|=============================|
|===   Working type 19:    ===|
|===  HeavyNeutrino_1.000  ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/Hnu_1.000                                                                                  
 
 Working on file   1 of   1: nts/heavynu/mumu_1gev.h                                                                              
 
 
|=============================|
|===   Working type 24:    ===|
|===  CohPi-               ===|
|=============================|
 
 UnitNum is:            1
 Datacard is: datacard/cohpiminnua                                                                                
 
 Working on file   1 of   3: nts/cohpimin/cohpim_bs_nuage_0_1.paw                                                                 Working on file   2 of   3: nts/cohpimin/cohpim_bs_nuage_0_2.paw                                                                 Working on file   3 of   3: nts/cohpimin/cohpim_bs_nuage_0_3.paw                                                                 
 
 
 **********************
 ***    JOB DONE    ***
 **********************
 
 
