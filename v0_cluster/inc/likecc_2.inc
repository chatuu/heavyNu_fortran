      integer ntnum
      integer maxnts
     
      parameter (maxnts = 1000)
      character*120 ntuplename(maxnts),qentname(maxnts)
      character*120 resntname(maxnts),ccdisntname(maxnts)
      character*120 ncdisntname(maxnts)
      character*120 cohppntname(maxnts),datantname(maxnts)

      integer mctype
      parameter (mctype = 5)
      integer icut
      parameter (icut = 30)
      integer id
      parameter (id = 500)
      integer nevents(maxnts),itype,mutot,nmutot

      integer nebin
      parameter (nebin = 8)

      character*100 variables(maxnts),vari_name(maxnts)
      real ncut(icut,6),cut400(20,6)
      real low_range(1000),high_range(1000),scale_factor(5)
      real qe_scale,res_scale,dis_scale
      real qe_factor,res_factor,dis_factor
      integer counter,qenum,resnum,ccdisnum,ncdisnum,cohppnum,datanum
      integer initial_count,last_count,event_count,ievents	
      integer outof_range(7,6),range_cut(6),loopcount,qeType
      real totalnumucc,nccc,nsdis,qeres,cohqe,qewrtres
      real genqe,genres,gencoh,genccdis,genncdis
      real qelscalefactor,resscalefactor,ccdisscalefactor
      real ncdisscalefactor,cohscalefactor
      real nevent
      real likecand1(10000)
      real likecand2(10000)
      real likecand3(10000)
      real likecand4(10000)
      real likecand5(10000)
      real likecand6(10000)
      real candbin1(21)
      real candbin2(21)
      real candbin3(21)
      real candbin4(21)
      real candbin5(21)
      real candbin6(21)
      real like1arr(20,20,20,mctype)
      integer inputlike1sig(20,20,20,1,1)
      integer inputlike1bak(20,20,20,1,1)
      integer smoothlike1sig(20,20,20,1,1)
      integer smoothlike1bak(20,20,20,1,1)
      real outputlike1sig(20,20,20,1,1)
      real outputlike1bak(20,20,20,1,1)
      real like1test(20,20,20,1,1)
      integer cevent
      real scales(mctype)
      real like2arr(20,20,20,mctype)
      integer inputlike2sig(20,20,20,1,1)
      integer inputlike2bak(20,20,20,1,1)
      integer smoothlike2sig(20,20,20,1,1)
      integer smoothlike2bak(20,20,20,1,1)
      real outputlike2sig(20,20,20,1,1)
      real outputlike2bak(20,20,20,1,1)
      real like2test(20,20,20,1,1)
      real evisbin(nebin)
      real zpi, ehad
      integer nvzeropass, nclupass
      real pxv01,pyv01,pzv01,ev01
      real pxv02,pyv02,pzv02,ev02
      real pxv0v0,pyv0v0,pzv0v0,ev0v0
      real pxclu1,pyclu1,pzclu1,eclu1
      real pxclu2,pyclu2,pzclu2,eclu2
      real pxcluclu,pycluclu,pzcluclu,ecluclu
      real pxv0clu,pyv0clu,pzv0clu,ev0clu
      real primpt1,primpt2,mpi0v0,mpi0clu,mpi0v0clu
      real xclu1,yclu1,ptclu1,outclu1,tclu1
      real xclu2,yclu2,ptclu2,outclu2,tclu2
      real tpi0v0,tpi0clu,tpi0v0clu,trhopv0,trhopclu,trhopv0clu
      real epi0v0,epi0clu,epi0v0clu,erhopv0,erhopclu,erhopv0clu
      real zpi0v0,zpi0clu,zpi0v0clu,zrhopv0,zrhopclu,zrhopv0clu
      real mrhopv0,mrhopclu,mrhopv0clu
      real mpion
      real mpionz
      real mrho
      real me
      parameter (mpion=0.13957018)
      parameter (mpionz=0.1349766)
      parameter (mrho=0.77549)
      parameter (me = 0.000511)

      data evisbin/ 0.0, 8.0, 15.0, 20.0, 30.0, 50.0, 100.0, 300.0/
     
      common /like1/ ntnum,ntuplename,qentname,resntname,ccdisntname
     &  ,ncdisntname,nevents,itype,qenum,resnum,ccdisnum,ncdisnum
     &  ,initial_count,last_count,event_count,ievents,cohppntname
     &  ,cohppnum,datanum,datantname
      
      common /like2/ ncut,mutot,nmutot

      common /like3/ counter,variables,vari_name,outof_range,
     +low_range,high_range,scale_factor,qe_scale,res_scale,dis_scale,
     +qe_factor,res_factor,dis_factor

      common /like4/ totalnumucc,nsdis,qeres,cohqe,genccdis,genncdis
     &  ,qewrtres,genqe,genres,gencoh,qelscalefactor,resscalefactor
     &  ,ccdisscalefactor,ncdisscalefactor,cohscalefactor

      common /testlike/ nevent,likecand1,likecand2,likecand3,
     +likecand4,likecand5,likecand6,candbin1,candbin2,candbin3
     +,candbin4,candbin5,candbin6,inputlike1sig,inputlike1bak
     +,cevent,like1arr,smoothlike1sig,smoothlike1bak
     +,outputlike1sig,outputlike1bak,like1test,loopcount,scales
     +,like2arr,inputlike2sig,inputlike2bak,smoothlike2sig
     +,smoothlike2bak,outputlike2sig,outputlike2bak
     +,like2test

      common /v0clurelated/ nvzeropass,nclupass,primpt1,primpt2,pxv01
     &  ,pyv01,pzv01,ev01,pxv02,pyv02,pzv02,ev02,pxv0v0,pyv0v0,pzv0v0
     &  ,ev0v0,pxclu1,pyclu1,pzclu1,eclu1,xclu1,yclu1,ptclu1,outclu1
     &  ,pxclu2,pyclu2,pzclu2,eclu2,tclu1,xclu2,yclu2,ptclu2,outclu2
     &  ,tclu2,pxcluclu,pycluclu,pzcluclu,ecluclu,pxv0clu,pyv0clu
     &  ,pzv0clu,ev0clu,mpi0v0,mpi0clu,mpi0v0clu,mrhopv0,mrhopclu
     &  ,mrhopv0clu,tpi0v0,tpi0clu,tpi0v0clu,trhopv0,trhopclu,trhopv0clu
     &  ,epi0v0,epi0clu,epi0v0clu,erhopv0,erhopclu,erhopv0clu,zpi0v0
     &  ,zpi0clu,zpi0v0clu,zrhopv0,zrhopclu,zrhopv0clu
