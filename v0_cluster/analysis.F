      subroutine analysis
      implicit none
      
#include "inc/likentuple_2.inc"
#include "inc/likecc_2.inc"      
      
      integer istat,idntuple,err,icycle,muontrack,phase2
      integer aa,bb,cc,dd,ee,ff,kk,qq,rr,ss,tt,uu,vv,id_new
      logical lc(20),lveto,ltube     
      
      real beam_weight,zmin
      real thetamuhad
      real temp

      real rpxhneur, rpyhneur, rpzhneur
      real pneu, asym_phadpneu

      real pxmu,pymu,pzmu,pmu,pxhad,pyhad,pzhad,phad
      real rpxlr(ncand),rpylr(ncand),rpzlr(ncand)
      real delpmu, delphad

      real mpi0cut

      id_new = 400
      
      loopcount=0
      initial_count = 1         ! first number
      last_count = qenum+resnum+ccdisnum+ncdisnum+cohppnum+datanum
      ntnum = (last_count - initial_count + 1) ! total numbers of ntuple
      
C read ntuples (sort)
      do event_count = initial_count,last_count
        nevent=0
        write (*,701),ntuplename(event_count)
 701    format(a100)
        
C open ntuple, set up working directory and define first 
C ntuple variables in each block
        call hropen(40,'nt',ntuplename(event_count),' ',4096,istat)
        call hcdir('//nt',' ')
        call hrin(id,999,0)
c **bing** use 999 means using the biggest cycle number 
        call hbname(id,' ',0,'$clear')
        call hbname(id,'header',run,'$set')
        call hbname(id,'mcgene',xvs,'$set')
        call hbname(id,'globrec',isele,'$set')
        call hbname(id,'cceverec',ncand,'$set')
        call hnoent(id,nevents(event_count))
        
C open nt event 
        do 1000 ievents = 1,nevents(event_count)
          pmu           = 0.0
          delpmu        = 0.0
          pxhad         = 0.0
          pyhad         = 0.0
          pzhad         = 0.0
          phad          = 0.0
          delphad       = 0.0
          pneu          = 0.0
          asym_phadpneu = 0.0
	  mpi0cut       = 0.0

          call hgnt(id,ievents,err)
          if (err .ne. 0) then
            print *,'error'            
          else
            if (mod(ievents,10000) .eq. 0) then
              write (*,702),'number of events process (500): '
     +          ,ievents,(loopcount +1)
     +          ,' time reading ntuple'
            else
            endif
 702        format(a31,i7,i7,a20)
            
c weight beam            
            beam_weight = 1.0
            if (event_count.ge.1 .and. event_count.le.qenum) then
              counter = 1
            else if (event_count.gt.qenum .and.
     +          event_count .le. (qenum+resnum)) then
              counter = 2
            else if (event_count.gt.(qenum+resnum) .and.
     +          event_count.le.(qenum+resnum+ccdisnum)) then
              counter = 3
            else if (event_count.gt.(qenum+resnum+ccdisnum) .and.
     +          event_count.le.(qenum+resnum+ccdisnum+ncdisnum)) then
              counter = 4
            else if (event_count.gt.(qenum+resnum+ccdisnum+ncdisnum)
     &          .and.event_count
     &          .le.(qenum+resnum+ccdisnum+ncdisnum+cohppnum)) then
              counter = 5
            else if (event_count
     &          .gt.(qenum+resnum+ccdisnum+ncdisnum+cohppnum) .and.
     +          event_count
     &          .le.(qenum+resnum+ccdisnum+ncdisnum+cohppnum+datanum)) 
     &          then
              counter = 6
            else
            endif
            
            if ((counter.ge.1) .and. (counter.le.5)) then
              call McWtDC_wts(1,6,zvs,1,beam_weight)
            else 
              beam_weight = 1.0
            endif

            if (counter.le.3) then
              beam_weight = beam_weight*0.700
            endif
            
c start 500 ntuple quality sample cuts
            ncut(1,counter) = ncut(1,counter) + beam_weight
            
c fermi momentum is greater than 1, then cut it.
            if (((sqrt(pxfermi**2 + pyfermi**2 + pzfermi**2)).gt.1.0) 
     &        .and. (counter.ne.6)) goto 100
            ncut(2,counter) = ncut(2,counter) + beam_weight
            
c for DIS, if w2s is less than 1.96, then cut it. 
            if (counter.eq.3 .and. w2s.lt.1.96) goto 100
            ncut(3,counter) = ncut(3,counter) + beam_weight
            
c vertex is out of fiducial, then cut it
            zmin = 5.0
            if (counter.eq.6) then
              if (run.le.8375)                    zmin = 265.0
              if (run.gt.8375 .and. run.le.9344)  zmin = 115.0
              if (run.gt.9344 .and. run.le.14164) zmin = 5.0
              if (run.gt.14164)                   zmin = 35.0
            endif
            
            if ((zvr.le.zmin .or. zvr.ge.405.0) .or.
     +        (xvr.le.-120.0 .or. xvr.ge.120.0) .or.
     +        (yvr.le.-115.0 .or. yvr.ge.125.0)) goto 100
            ncut(4,counter) = ncut(4,counter) + beam_weight
            
c select phase2 muon only
            call label_muid(phase2,muontrack)
            if (phase2.lt.1) goto 100
            ncut(5,counter) = ncut(5,counter) + beam_weight
            
c impose tube and veto cut
            call dcveto(ncand,totveto(muontrack),tottube(muontrack),
     +        lveto,ltube)
            if (.not.lveto .or. .not.ltube) goto 100
            ncut(6,counter) = ncut(6,counter) + beam_weight
            
c select nrpim=2 events
            if (ncand.ne.2) goto 100
            ncut(7,counter) = ncut(7,counter) + beam_weight
            
c select mu- events
            if (lepq(muontrack).ne.-1) goto 100
            ncut(8,counter) = ncut(8,counter) + beam_weight
            
c select had+ events
            do aa = 1,ncand
              if (aa.ne.muontrack) then
                if (lepq(aa).ne.1) goto 100
              endif
            enddo
            ncut(9,counter) = ncut(9,counter) + beam_weight
            
c select events with thetamuhad < 177.5
            thetamuhad = 0.0
            call angle_cal(thetamuhad, pxlr(1), pylr(1), pzlr(1),
     +        pxlr(2), pylr(2), pzlr(2))
            thetamuhad = thetamuhad*180/3.14
            if (thetamuhad.gt.177.5) goto 100
            ncut(10,counter) = ncut(10,counter) + beam_weight

c select had+ events with momentum great than 1.0 GeV
c rotate to beam frame
            do aa = 1,ncand
              rpxlr(aa) = pxlr(aa)
              call labtobeam(pxlr(aa),pylr(aa),pzlr(aa),rpylr(aa)
     &          ,rpzlr(aa))
            enddo
            
            pmu    = sqrt(rpxlr(muontrack)**2+rpylr(muontrack)**2
     &        + rpzlr(muontrack)**2)
            delpmu = deltap(muontrack)
            
            do aa = 1,ncand
              if (aa.ne.muontrack) then
                pxhad   = pxhad+rpxlr(aa)
                pyhad   = pyhad+rpylr(aa)
                pzhad   = pzhad+rpzlr(aa)
                phad    = phad+sqrt(pxhad**2 + pyhad**2 + pzhad**2)
                delphad = delphad + deltap(aa)
              endif
            enddo

            if (delpmu/pmu.gt.0.3
     &        .or. delphad/phad.gt.0.5
     &        .or. evis(1).gt.300.) then
              goto 100
            endif
            ncut(11,counter) = ncut(11,counter) + beam_weight

            if (sqrt(phad**2+mpion**2).lt.1.0) goto 100
            ncut(12,counter) = ncut(12,counter) + beam_weight

C Require ThMuPi<0.5 Rad.
            if (thetamuhad.gt.90./3.14) goto 100
            ncut(13,counter) = ncut(13,counter) + beam_weight

C Require mPt<0.5 GeV
            if (ptmis(muontrack).gt.0.5) goto 100
            ncut(14,counter) = ncut(14,counter) + beam_weight            

C Require asym_phadpneu>0.0
            rpxhneur = pxhneur
            call labtobeam(pxhneur,pyhneur,pzhneur,rpyhneur,rpzhneur)
            pneu = sqrt(rPxHNeuR**2 + rPyHNeuR**2 + rPzHNeuR**2)
            asym_phadpneu    = (phad-pneu)/(phad+pneu)      
            if (asym_phadpneu.lt.0.) goto 100
            ncut(15,counter) = ncut(15,counter) + beam_weight
         
            call v0_cluster(pxhad,pyhad,pzhad,phad)
            if (nvzeropass.eq.2 .and. nclupass.eq.0) then
              mpi0cut = mpi0v0
            elseif (nvzeropass.eq.1 .and. nclupass.eq.1) then
              mpi0cut = mpi0v0clu
            elseif (nvzeropass.eq.0 .and. nclupass.eq.2) then
              mpi0cut = mpi0clu
            else
C              print *, "There is no V0 or Cluster"
            endif
            
            if (mpi0cut.gt.0.05) goto 100
            ncut(16,counter) = ncut(16,counter) + beam_weight
            
c start ntanalysis
            call sortanalysis
c******bing****** this sortanalysis donot sort anything, but fill (only signal:cohpi+) likecand1 to likecand5, and fill ntvlist(1-15)
            
          endif
          
 100      continue
 1000   continue ! loop for each entry in single ntuple file
        
c close ntuple
        call hrend('nt')
        close(40)
        
      enddo                     ! read next ntuple file
      
c computer mc scale factors
      qelscalefactor   = cut400(5,1)/genqe
      resscalefactor   = cut400(5,2)/genres
      ccdisscalefactor = cut400(5,3)/genccdis
      ncdisscalefactor = cut400(5,4)/genncdis
      cohscalefactor   = cut400(5,5)/gencoh
      
      scales(1)      = qelscalefactor
      scales(2)      = resscalefactor
      scales(3)      = ccdisscalefactor
      scales(4)      = ncdisscalefactor
      scales(5)      = cohscalefactor
      
      print *, 'scales 5=', scales(5)

c find likelihood candidates bin
      call sorting(likecand1,candbin1)
      call sorting(likecand2,candbin2)
      call sorting(likecand3,candbin3)
      call sorting(likecand4,candbin4)
      call sorting(likecand5,candbin5)
      
      open (50,file='likecandbin.txt',status="unknown")
      do aa = 1,21
        write (50,310),candbin1(aa),candbin2(aa),candbin3(aa)
     +    ,candbin4(aa),candbin5(aa)
      enddo
 310  format(5f10.5)
      close(50)
      
      loopcount = 0
10000 continue
      do aa = 1,30
        do bb = 1,6
          ncut(aa,bb) = 0.0
        enddo
      enddo      
      
c read ntuples again (analysis)
      do event_count = initial_count,last_count
        nevent = 0
        write (*,701),ntuplename(event_count)
        
c open ntuple, set up working directory and define first 
c ntuple variables in each block
        call hropen(40,'nt',ntuplename(event_count),' ',4096,istat)
        call hcdir('//nt',' ')
        call hrin(id,999,0)
        call hbname(id,' ',0,'$clear')
        call hbname(id,'header',run,'$set')
        call hbname(id,'mcgene',xvs,'$set')
        call hbname(id,'globrec',isele,'$set')
        call hbname(id,'cceverec',ncand,'$set')
        call hnoent(id,nevents(event_count))
        
        do 1001 ievents = 1,nevents(event_count)
          pmu           = 0.0
          delpmu        = 0.0
          pxhad         = 0.0
          pyhad         = 0.0
          pzhad         = 0.0
          phad          = 0.0
          delphad       = 0.0
          pneu          = 0.0
          asym_phadpneu = 0.0
          mpi0cut       = 0.0

          call hgnt(id,ievents,err)
          if (err .ne. 0) then
            print *,'error'            
          else
            if (mod(ievents,10000) .eq. 0) then
              write (*,702),'number of events process (500): '
     +          ,ievents,(loopcount +1)
     +          ,' time reading ntuple'
            else
            endif
            
            beam_weight = 1.0            
            if (event_count.ge.1 .and. event_count.le.qenum) then
              counter = 1
            else if (event_count.gt.qenum .and.
     +          event_count.le.(qenum+resnum)) then
              counter = 2
            else if (event_count.gt.(qenum+resnum) .and.
     +          event_count.le.(qenum+resnum+ccdisnum)) then
              counter = 3
            else if (event_count.gt.(qenum + resnum + ccdisnum) .and.
     +          event_count.le.(qenum+resnum+ccdisnum+ncdisnum)) then
              counter = 4
            else if (event_count.gt.(qenum+resnum+ccdisnum+ncdisnum) 
     &          .and. event_count.le.
     &          (qenum+resnum+ccdisnum+ncdisnum+cohppnum)) then
              counter = 5
            else if (event_count.gt.
     &          (qenum+resnum+ccdisnum+ncdisnum+cohppnum)
     &          .and. event_count.le.
     &          (qenum+resnum+ccdisnum+ncdisnum+cohppnum+datanum)) then
              counter = 6
            else
            endif
            
            if ((counter.ge.1) .and. (counter.le.5)) then
              call McWtDC_wts(1,6,zvs,1,beam_weight)
            else 
              beam_weight = 1.0
            endif

            if (counter.le.3) then
              beam_weight = beam_weight*0.700
            endif
            
c start 500 ntuple quality sample cuts
            ncut(1,counter) = ncut(1,counter) + beam_weight
            
c fermi momentum is greater than 1, then cut it.
            if (((sqrt(pxfermi**2+pyfermi**2+pzfermi**2)).gt.1.0) .and.
     &        (counter.ne.6)) goto 130
            ncut(2,counter) = ncut(2,counter) + beam_weight
            
c for DIS, if w2s is less than 1.96, then cut it. 
            if (counter.eq.3 .and. w2s.lt.1.96) goto 130
            ncut(3,counter) = ncut(3,counter) + beam_weight
            
c vertex is out of fiducial, then cut it
            zmin = 5.0
            if (counter.eq.6) then
              if (run.le.8375)                    zmin = 265.0
              if (run.gt.8375 .and. run.le.9344)  zmin = 115.0
              if (run.gt.9344 .and. run.le.14164) zmin = 5.0
              if (run.gt.14164)                   zmin = 35.0
            endif
            
            if ((zvr.le.zmin .or. zvr.ge.405.0) .or.
     +        (xvr.le.-120.0 .or. xvr.ge.120.0) .or.
     +        (yvr.le.-115.0 .or. yvr.ge.125.0)) goto 130
            ncut(4,counter) = ncut(4,counter) + beam_weight
            
c select phase2 muon only
            call label_muid(phase2,muontrack)
            if (phase2.lt.1) goto 130
            ncut(5,counter) = ncut(5,counter) + beam_weight
            
c impose tube and veto cut
            call dcveto(ncand,totveto(muontrack),tottube(muontrack),
     +        lveto,ltube)
            if (.not.lveto .or. .not.ltube) goto 130
            ncut(6,counter) = ncut(6,counter) + beam_weight
            
c select nrpim=2 events
            if (ncand.ne.2) goto 130
            ncut(7,counter) = ncut(7,counter) + beam_weight
            
c select mu- events
            if (lepq(muontrack).ne.-1) goto 130
            ncut(8,counter) = ncut(8,counter) + beam_weight
            
c select had+ events
            do aa = 1,ncand
              if (aa.ne.muontrack) then
                if (lepq(aa).ne.1) goto 130
              endif
            enddo
            ncut(9,counter) = ncut(9,counter) + beam_weight
            
c select events with thetamuhad < 177.5
            thetamuhad = 0.0
            call angle_cal(thetamuhad, pxlr(1), pylr(1), pzlr(1),
     +        pxlr(2), pylr(2), pzlr(2))
            thetamuhad = thetamuhad*180/3.14
            if (thetamuhad.gt.177.5) goto 130
            ncut(10,counter) = ncut(10,counter) + beam_weight

C select had+ events with momentum great than 1.0 GeV
c rotate to beam frame
            do aa = 1,ncand
              rpxlr(aa) = pxlr(aa)
              call labtobeam(pxlr(aa),pylr(aa),pzlr(aa),rpylr(aa)
     &          ,rpzlr(aa))
            enddo

            pmu    = sqrt(rpxlr(muontrack)**2+rpylr(muontrack)**2
     &        + rpzlr(muontrack)**2)
            delpmu = deltap(muontrack)

            do aa = 1,ncand
              if (aa.ne.muontrack) then
                pxhad   = pxhad+rpxlr(aa)
                pyhad   = pyhad+rpylr(aa)
                pzhad   = pzhad+rpzlr(aa)
                phad    = phad+sqrt(pxhad**2 + pyhad**2 + pzhad**2)
                delphad = delphad + deltap(aa)
              endif
            enddo

            if (delpmu/pmu.gt.0.3
     &        .or. delphad/phad.gt.0.5
     &        .or. evis(1).gt.300.) then
              goto 130
            endif
            ncut(11,counter) = ncut(11,counter) + beam_weight

            if (sqrt(phad**2+mpion**2).lt.1.0) goto 130
            ncut(12,counter) = ncut(12,counter) + beam_weight

C Require ThMuPi<0.5 Rad.
            if (thetamuhad.gt.90./3.14) goto 130
            ncut(13,counter) = ncut(13,counter) + beam_weight

C Require mPt<0.5 GeV
            if (ptmis(muontrack).gt.0.5) goto 130
            ncut(14,counter) = ncut(14,counter) + beam_weight            
            
C Require asym_phadpneu>0.0
            rpxhneur = pxhneur
            call labtobeam(pxhneur,pyhneur,pzhneur,rpyhneur,rpzhneur)
            pneu = sqrt(rPxHNeuR**2 + rPyHNeuR**2 + rPzHNeuR**2)
            asym_phadpneu    = (phad-pneu)/(phad+pneu)      
            if (asym_phadpneu.lt.0.) goto 130
            ncut(15,counter) = ncut(15,counter) + beam_weight            

            call v0_cluster(pxhad,pyhad,pzhad,phad)
            if (nvzeropass.eq.2 .and. nclupass.eq.0) then
              mpi0cut = mpi0v0
            elseif (nvzeropass.eq.1 .and. nclupass.eq.1) then
              mpi0cut = mpi0v0clu
            elseif (nvzeropass.eq.0 .and. nclupass.eq.2) then
              mpi0cut = mpi0clu
            else
C              print *, "There is no V0 or Cluster"
            endif
            
            if (mpi0cut.gt.0.05) goto 130
            ncut(16,counter) = ncut(16,counter) + beam_weight

c start ntanalysis
cc*****bing****** eventanalysis function (with the whole loop) will get called more than once!!!
cc                the first time ,loopcount=0, like1arr(ii,jj,kk,counter)  will get filled
cc               the second time, loopcount=1, fill many histograms, and create likecand6(with first 10000 events like1test value)
cc                third:loopcount=2, for  like1test(ii,jj,kk,1,1) of each event , find the position in new created candbin6, and create like2arr(ll,mm,nn,counter)
cc                fourth: loopcount=3, with new created like2test, fill some histograms
            
            call eventanalysis(beam_weight)
          endif
          
 130      continue
 1001   continue
        
c close ntuple
        call hrend('nt')
        close(40)
        
      enddo                     ! read next ntuple file
      
      if (loopcount .eq. 1) goto 11000
      if (loopcount .eq. 2) goto 12000
      if (loopcount .eq. 3) goto 13000
      
c compute likelihood function
      open (50, file='lh1-3d.txt', status="unknown")
      do aa = 1,20
        do bb = 1,20
          do cc = 1,20
            inputlike1sig(aa,bb,cc,1,1) = int(like1arr(aa,bb,cc,5))
            inputlike1bak(aa,bb,cc,1,1) = int(
     &        (like1arr(aa,bb,cc,1)/qelscalefactor)+
     &        (like1arr(aa,bb,cc,2)/resscalefactor)+
     &        (like1arr(aa,bb,cc,3)/ccdisscalefactor)+
     &        (like1arr(aa,bb,cc,4)/ncdisscalefactor))
          enddo
        enddo
      enddo
      
c smooth the likelihood arrays
      call smoother(20,20,20,1,1,inputlike1sig,smoothlike1sig)
      call smoother(20,20,20,1,1,inputlike1bak,smoothlike1bak)
      
      do aa = 1,20
        do bb = 1,20
          do cc = 1,20
            outputlike1sig(aa,bb,cc,1,1)=
     &        real(smoothlike1sig(aa,bb,cc,1,1))/2000.0
            outputlike1bak(aa,bb,cc,1,1)=
     &        real(smoothlike1bak(aa,bb,cc,1,1))/2000.0
            
          enddo
        enddo
      enddo
      
      do aa = 1,20
        do bb = 1,20
          do cc = 1,20
c         if (outputlike1bak(aa,bb,cc,1,1) .eq. 0.0) then
c            outputlike1bak(aa,bb,cc,1,1)=0.0001
c            print *,'aaa'
c         endif
            like1test(aa,bb,cc,1,1) = 0.0
            temp = 0.0
            temp = outputlike1sig(aa,bb,cc,1,1)
     &        -outputlike1bak(aa,bb,cc,1,1)      
            like1test(aa,bb,cc,1,1) = temp
            
c         write(50,311),outputlike1sig(aa,bb,cc,1,1),
c     +   outputlike1bak(aa,bb,cc,1,1)
            write(50,311),like1test(aa,bb,cc,1,1)
cc**** write to lh1-3d.txt 
 311        format(f15.5)
            
          enddo
        enddo
      enddo
      
      close(50)
      
      loopcount = loopcount+1
      goto 10000
      
11000 continue
      
c sort likelihood value in an 10000 array
      call sorting(likecand6,candbin6)
      open(50,file='candbin6.txt',status="unknown")
      do aa = 1,21
        write (50,312),candbin6(aa)
      enddo
 312  format(f10.5)
      close(50)
      
      loopcount = loopcount+1
      goto 10000
      
c compute secnond likelihood
12000 continue
      
      open(50,file='lh2-3d.txt', status="unknown")
      do aa = 1,20
        do bb = 1,20
          do cc = 1,20
            inputlike2sig(aa,bb,cc,1,1) = int(like2arr(aa,bb,cc,5))
            inputlike2bak(aa,bb,cc,1,1) = int(
     &        (like2arr(aa,bb,cc,1)/qelscalefactor)+
     &        (like2arr(aa,bb,cc,2)/resscalefactor)+
     &        (like2arr(aa,bb,cc,3)/ccdisscalefactor)+
     &        (like2arr(aa,bb,cc,4)/ncdisscalefactor))
            
          enddo
        enddo
      enddo
      
c smooth the likelihood arrays
      call smoother(20,20,20,1,1,inputlike2sig,smoothlike2sig)
      call smoother(20,20,20,1,1,inputlike2bak,smoothlike2bak)
      
      do aa = 1,20
        do bb = 1,20
          do cc = 1,20
            outputlike2sig(aa,bb,cc,1,1) =
     &        real(smoothlike2sig(aa,bb,cc,1,1))/2000.0
            outputlike2bak(aa,bb,cc,1,1) =
     &        real(smoothlike2bak(aa,bb,cc,1,1))/2000.0
            
          enddo
        enddo
      enddo
      
      do aa = 1,20
        do bb = 1,20
          do cc = 1,20
c         if (outputlike2bak(aa,bb,cc,1,1) .eq. 0.0) then
c            outputlike2bak(aa,bb,cc,1,1)=0.0001
c            print *,'aaa'
c         endif
            like2test(aa,bb,cc,1,1) = 0.0
            temp                    = 0.0
            temp                    = outputlike2sig(aa,bb,cc,1,1)
     &        -outputlike2bak(aa,bb,cc,1,1)      
            like2test(aa,bb,cc,1,1) = temp
            
c         write(50,311),outputlike2sig(aa,bb,cc,1,1),
c     +   outputlike2bak(aa,bb,cc,1,1)
            write(50,311),like2test(aa,bb,cc,1,1)
            
          enddo
        enddo
      enddo
      close(50)
      loopcount = loopcount+1
      goto 10000
      
13000 continue
      
      return
      end
      
