      subroutine angle_cal2(angle,x1,x2,x3,y1,y2,y3)
      implicit none

      real angle,dotpro
      real x1,x2,x3,y1,y2,y3
      real totalx,totaly

      totalx = sqrt(x1**2 + x2**2 + x3**2)
      totaly = sqrt(y1**2 + y2**2 + y3**2)

      dotpro = x1*y1 + x2*y2 + x3*y3

      if (totalx .eq. 0.0 .or. totaly .eq. 0.0) then

         angle = 100.0
         goto 70

      else
      endif

      angle = acos(dotpro / (abs(totalx) * abs(totaly)))

 70   continue
      
      return
      end
